package com.icoding.imall.customer.mapper;


import com.icoding.imall.api.bean.ImProduct;
import com.icoding.imall.api.service.MyMapper;
import org.springframework.stereotype.Repository;

@Repository
public interface ImProductMapper extends MyMapper<ImProduct> {
}