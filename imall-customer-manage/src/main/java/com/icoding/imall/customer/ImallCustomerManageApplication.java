package com.icoding.imall.customer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@ComponentScan("com.icoding.imall")
@MapperScan("com.icoding.imall.customer.mapper")
public class ImallCustomerManageApplication {

    public static void main(String[] args) {
        SpringApplication.run(ImallCustomerManageApplication.class, args);
    }

}
