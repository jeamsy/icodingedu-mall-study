package com.icoding.imall.customer.mapper;

import com.icoding.imall.api.bean.ImProductVariants;
import com.icoding.imall.api.service.MyMapper;
import org.springframework.stereotype.Repository;

@Repository
public interface ImProductVariantsMapper extends MyMapper<ImProductVariants> {

}