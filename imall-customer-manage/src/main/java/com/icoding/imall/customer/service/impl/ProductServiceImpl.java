package com.icoding.imall.customer.service.impl;

import com.github.pagehelper.PageHelper;
import com.icoding.imall.api.bean.ImProduct;
import com.icoding.imall.api.service.ProductService;
import com.icoding.imall.customer.mapper.ImProductMapper;
import org.apache.dubbo.config.annotation.Service;
import org.apache.dubbo.rpc.RpcContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Service(version = "3.0.0")
@Component
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ImProductMapper imProductMapper;

    @Override
    public List<ImProduct> queryProductList(int pageNum, int pageSize) {
        System.out.println("***********manage 3.0.0***********");
        String appid = RpcContext.getContext().getAttachment("appid");
        System.out.println(appid);

        PageHelper.startPage(pageNum,pageSize);
        List<ImProduct> imProductList = imProductMapper.selectAll();
        return imProductList;
    }

}
