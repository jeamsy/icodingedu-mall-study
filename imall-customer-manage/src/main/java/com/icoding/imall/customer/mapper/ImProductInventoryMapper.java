package com.icoding.imall.customer.mapper;


import com.icoding.imall.api.bean.ImProductInventory;
import com.icoding.imall.api.service.MyMapper;
import org.springframework.stereotype.Repository;

@Repository
public interface ImProductInventoryMapper extends MyMapper<ImProductInventory> {
}