package com.icoding.imall.customer.controller;

import com.icoding.imall.api.handle.R;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @GetMapping("sayHello")
    public R sayHello(){

        return R.returnOK(0,"hello:8092");
    }
}
