package com.icoding.imall.customer.service.impl;

import com.icoding.imall.api.bean.ImProduct;
import com.icoding.imall.api.service.ProductService;

import java.util.List;

public class CustomerServiceStub implements ProductService {

    //远端调用的服务
    private final ProductService productService;
    //dubbo会通过这个有参的构造函数注入远程调用
    public CustomerServiceStub(ProductService productService) {
        this.productService = productService;
    }

    @Override
    public List<ImProduct> queryProductList(int pageNum, int pageSize) {

        System.out.println("你可以在这里操作东西。。。。。。");

        return productService.queryProductList(pageNum, pageSize);
    }
}
