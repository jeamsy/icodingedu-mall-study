package com.icoding.imall.customer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.icoding.imall")
public class ImallAdminWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(ImallAdminWebApplication.class, args);
    }
}
