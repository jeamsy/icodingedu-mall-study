package com.icoding.imall.customer.service.impl;


import com.icoding.imall.api.bean.ImProduct;
import com.icoding.imall.api.service.ProductService;

import java.util.ArrayList;
import java.util.List;

public class CustomerServiceMock implements ProductService {

    @Override
    public List<ImProduct> queryProductList(int pageNum, int pageSize) {
        List<ImProduct> productList = new ArrayList<>();
        ImProduct dto = new ImProduct();
        dto.setFirstImageSrc("http://www.baidu.com");
        dto.setDetailDescription("mock");
        productList.add(dto);
        return productList;
    }
}
