package com.icoding.imall.customer.controller;

import com.icoding.imall.api.handle.R;
import com.icoding.imall.api.service.ProductService;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.rpc.RpcContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @Reference(mock = "com.icoding.imall.customer.service.impl.CustomerServiceMock",
            stub = "com.icoding.imall.customer.service.impl.CustomerServiceStub",version = "3.0.0")
    private ProductService productService;

    @GetMapping("sayHello")
    public R sayHello(){

        return R.returnOK(0,"hello:8091");
    }

    @GetMapping("getProductList")
    public R getProductList(){

        RpcContext.getContext().setAttachment("appid","shaojian");
        return R.returnOK().returnOK(1,"",productService.queryProductList(1,5));
    }
}
