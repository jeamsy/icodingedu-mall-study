package com.icoding.imall.api.service;

import com.icoding.imall.api.bean.ImProduct;

import java.util.List;

public interface ProductService {
    List<ImProduct> queryProductList(int pageNum, int pageSize);
}
