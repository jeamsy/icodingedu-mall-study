**imall-parent**

管理所有项目的包的版本控制

**imall-api**

im-api模块也是创建一个maven的module，主要用来存放bean数据和后续所有业务模块的接口

将bean和接口抽离是为了使各个业务模块更面向业务，将公共使用的实体类、pojo、接口集中管理，便于进行服务注册的发现的调用

由于要存放bean，因此需要导入的Lombok依赖和后面tk-mybatis生成pojo的依赖

**imall-common-util**

存放所有业务模块公共的依赖（springboot启动依赖）以及工具类，统一API返回工具类，统一异常输出

**imall-web-util**

存放对接静态前端页面的controller相关依赖，例如我们的后台管理系统界面使用thymeleaf进行开发，这里就需要引入thymeleaf的依赖

**imall-service-util**

存放后端业务服务模块相关依赖包，比如数据库依赖，MyBatis，PageHelper

**imall-admin-web**

后台管理系统业务模块，使用thymeleaf集成功能页面，主要包含：鉴权，权限管理，员工管理，thymeleaf页面

因此基础模块都需要导入，**对于业务系统均需要创建为springboot项目**

**imall-customer-manage**

产品信息管理模块，对外提供产品相关信息的查询上传等功能，输入后端服务

因此只需要引用：im-parent, im-api, im-common-util, im-service-util，**依然是springboot工程**